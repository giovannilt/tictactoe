//
//  UserPlayer.h
//  TicTacToe
//
//  Created by Lion User on 20/09/2012.
//  Copyright (c) 2012 MensAgilis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Sings.h"
#import "Move.h"
#import "IBoard.h"

@interface UserPlayer : NSObject

@property NSString * name;
@property Sings * sing;

-(Move *) getNextMove: (IBoard *) board;

@end
