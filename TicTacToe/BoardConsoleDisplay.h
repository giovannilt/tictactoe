//
//  BoardConsoleDisplay.h
//  TicTacToe
//
//  Created by Lion User on 20/09/2012.
//  Copyright (c) 2012 MensAgilis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IBoard.h"

@interface BoardConsoleDisplay : NSObject

@property (readwrite) IBoard * board;

-(void) display;

@end
