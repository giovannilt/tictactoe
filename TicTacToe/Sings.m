//
//  Sings.m
//  TicTacToe
//
//  Created by Lion User on 19/09/2012.
//  Copyright (c) 2012 MensAgilis. All rights reserved.
//

#import "Sings.h"

@implementation Sings

@synthesize segno;

-(id) initWithSegno:(enum Sing) sing{
    if((self = [super init])){
        segno=sing;
    }
    return self;
}
-(BOOL) isEqualTo:(id) object{
    if([object isKindOfClass:[Sings class]]){
        return [self segno] == [object segno];
    }else{
        return NO;
    }
}
-(BOOL) isItBlank{
    return [self segno] == Blank;
}

@end
