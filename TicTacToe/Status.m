//
//  Status.m
//  TicTacToe
//
//  Created by Lion User on 19/09/2012.
//  Copyright (c) 2012 MensAgilis. All rights reserved.
//

#import "Status.h"

@implementation Status

@synthesize state;

-(NSString *)description{
    switch ([self state]) {
        case  GameOpen:
            return @"GameOpen";
            break;
        case CrossWin:
            return @"CrossWin";
            break;
        case CircleWin:
            return @"CircleWin";
            break;
        default:
            return @"Draw";
            break;
    }
}

@end
