//
//  main.m
//  TicTacToe
//
//  Created by Lion User on 19/09/2012.
//  Copyright (c) 2012 MensAgilis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserPlayer.h"
#import "IBoard.h"
#import "BoardConsoleDisplay.h"

UserPlayer *p1;
UserPlayer *p2;
UserPlayer *currPlayer;
IBoard *board;
BoardConsoleDisplay *display;


void displayHelp(){
    NSMutableString *buffer= [NSMutableString stringWithFormat:@"\r ===== THIS GAME (HELP) ==== \r\r"];
    
    [buffer appendFormat:@"[0,0] | [0,1] | [0,2]\r"];
    [buffer appendFormat:@"---------------------\r"];
    [buffer appendFormat:@"[1,0] | [1,1] | [1,2]\r"];
    [buffer appendFormat:@"---------------------\r"];
    [buffer appendFormat:@"[2,0] | [2,1] | [2,2]\r\r\r"];
    
    NSLog(@"%@", buffer);
}

BOOL isGameEnded(){
    return [[board getStatus] state]!= GameOpen || [[board getStatus] state] == Draw;
}

void go(){
    [display display];
    if(isGameEnded()){
        NSLog(@"Game Ended result: %@", [board getStatus]);
        return;
    }else{
        NSLog(@"Player %@ moves.",[currPlayer name]);
        Move *m= [currPlayer getNextMove: [board clone]];
        
        @try {
            [board setCell:[m row] in:[m column] sign: [currPlayer sing]];
            currPlayer= currPlayer==p1 ? p2 : p1;
        }
        @catch (NSException *exception) {
            NSLog(@"ERROR..... %@", exception);
            //return;
        }
        go();
    }
}

int main(int argc, const char * argv[]){
    @autoreleasepool {
        
        NSLog(@"Benvenuto nel TIC TAC TOE!");
        
        p1=[[UserPlayer alloc]init];
        [p1 setName:@"Player1"];
        [p1 setSing:[[Sings alloc]initWithSegno:Cross]];
        
        p2=[[UserPlayer alloc]init];
        [p2 setName:@"Player2"];
        [p2 setSing:[[Sings alloc]initWithSegno:Circle]];
        
        
        currPlayer=[[UserPlayer alloc]init];
        
        currPlayer= p1;
        
        board=[[IBoard alloc]init];
        display=[[BoardConsoleDisplay alloc]init];
        [display setBoard:board];
        
        displayHelp();
      
        go();
        
        
    }
    return 0;
}

