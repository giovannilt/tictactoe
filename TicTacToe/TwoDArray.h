//
//  TwoDArray.h
//  TicTacToe
//
//  Created by Lion User on 20/09/2012.
//  Copyright (c) 2012 MensAgilis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Sings.h"

@interface TwoDArray : NSObject

-(void) setIn:(int) row col: (int) column val:(Sings *) sign;
-(Sings *) getSign: (int) row col: (int)col;


@end
