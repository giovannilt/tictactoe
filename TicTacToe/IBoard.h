//
//  IBoard.h
//  TicTacToe
//
//  Created by Lion User on 19/09/2012.
//  Copyright (c) 2012 MensAgilis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Sings.h"
#import "Status.h"
#import "TwoDArray.h"

@interface IBoard : NSObject

-(id) initWithBoard:(TwoDArray *) board;

-(Sings *) getCell:(int) row in:(int) column;

-(void) setCell:(int) row in:(int) column sign:(Sings *) sing;

-(Status *) getStatus;

-(IBoard *) clone;  

@end
