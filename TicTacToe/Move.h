//
//  Move.h
//  TicTacToe
//
//  Created by Lion User on 20/09/2012.
//  Copyright (c) 2012 MensAgilis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Move : NSObject

@property (readwrite)int row;
@property (readwrite)int column;

@end
