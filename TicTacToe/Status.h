//
//  Status.h
//  TicTacToe
//
//  Created by Lion User on 19/09/2012.
//  Copyright (c) 2012 MensAgilis. All rights reserved.
//

#import <Foundation/Foundation.h>

enum State{
    GameOpen,
    CrossWin,
    CircleWin,
    Draw
};


@interface Status : NSObject

@property (readwrite) enum State state;

@end
