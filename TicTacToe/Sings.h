//
//  Sings.h
//  TicTacToe
//
//  Created by Lion User on 19/09/2012.
//  Copyright (c) 2012 MensAgilis. All rights reserved.
//

#import <Foundation/Foundation.h>

enum Sing{
    Blank,
    Cross,
    Circle
};


@interface Sings : NSObject

-(id) initWithSegno:(enum Sing) sign;

-(BOOL) isItBlank;

@property (readwrite) enum Sing segno;

@end
