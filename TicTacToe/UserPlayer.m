//
//  UserPlayer.m
//  TicTacToe
//
//  Created by Lion User on 20/09/2012.
//  Copyright (c) 2012 MensAgilis. All rights reserved.
//

#import "UserPlayer.h"

@implementation UserPlayer

@synthesize name, sing;

-(Move *) getNextMove: (IBoard *) board{
    int row, column;
    NSLog(@"Enter ROW:");
    scanf("%i", &row);
    NSLog(@"Enter COLUMN:");
    scanf("%i", &column);
    
    Move *m = [[Move alloc]init];
    [m setRow:row];
    [m setColumn:column];
    
    return m;
}

@end
