//
//  TwoDArray.m
//  TicTacToe
//
//  Created by Lion User on 20/09/2012.
//  Copyright (c) 2012 MensAgilis. All rights reserved.
//

#import "TwoDArray.h"

@interface TwoDArray(){
    NSMutableArray *r1;
    NSMutableArray *r2;
    NSMutableArray *r3;
    
}
@end

@implementation TwoDArray

-(id) init{
    if(self=[super init]){
        r1= [NSMutableArray arrayWithCapacity:3];
        r2= [NSMutableArray arrayWithCapacity:3];
        r3= [NSMutableArray arrayWithCapacity:3];
        for( int i=0; i<3;i++){
            [r1 addObject:[[Sings alloc] initWithSegno: Blank]];
            [r2 addObject:[[Sings alloc] initWithSegno: Blank]];
            [r3 addObject:[[Sings alloc] initWithSegno: Blank]];
        }
    }
    return self;
}

-(void) setIn:(int) row col: (int) column val:(Sings *) sign{
    if(row<0 || row>3){
        @throw [NSException exceptionWithName:@"Invalid Parameter" reason:@"Row must be between 0 and 2" userInfo:nil];
    }
    if(column<0 || column>3){
        @throw [NSException exceptionWithName:@"Invalid Parameter" reason:@"COL must be between 0 and 2" userInfo:nil];
    }
    switch (row) {
        case 0:
            [r1 insertObject:sign atIndex:column];
            break;
        case 1:
            [r2 insertObject:sign atIndex:column];
            break;
        default:
            [r3 insertObject:sign atIndex:column];
            break;
    };
}

-(Sings *) getSign: (int) row col: (int)col{
    
    if(row<0 || row>3){
        @throw [NSException exceptionWithName:@"Invalid Parameter" reason:@"Row must be between 0 and 2" userInfo:nil];
    }
    if(col<0 || col>3){
        @throw [NSException exceptionWithName:@"Invalid Parameter" reason:@"COL must be between 0 and 2" userInfo:nil];
    }
    switch (row) {
        case 0:
            return [r1 objectAtIndex:col];
            break;
        case 1:
            return [r2 objectAtIndex:col];
            break;
        default:
            return [r3 objectAtIndex:col];
            break;
    };
    
}


@end
