//
//  IBoard.m
//  TicTacToe
//
//  Created by Lion User on 19/09/2012.
//  Copyright (c) 2012 MensAgilis. All rights reserved.
//
#import "IBoard.h"

@interface IBoard(){
    TwoDArray *board;
}
@end

@implementation IBoard

-(id) init{
    if(self = [super init]){
        board=[[TwoDArray alloc] init];
    }
    return self;
}
-(id) initWithBoard: (TwoDArray*) boardCopy{
    if(self =[super init]){
        for(int row=0; row<3;row++){
            for(int column=0; column<3; column++){
                [board setIn:row col:column val:[boardCopy getSign:row col:column]];
                break;
            }
        }
    }
    return self;
}

-(Sings *) getCell:(int)row in:(int)column{
    [self checkValue: row versus: @"Row"];
    [self checkValue: column versus: @"Columnt"];
    return [board getSign:row col:column];
}

-(void) setCell:(int)row in:(int)column sign:(Sings *)sing{
    [self checkValue: row versus: @"Row"];
    [self checkValue: column versus: @"Columnt"];
    if([[board getSign:row col:column] segno] == Blank){
        [board setIn:row col:column val:sing];
    }else{
        @throw ([NSException exceptionWithName:@"InvalidAssignmentException" reason:[NSString stringWithFormat:@"Row %i Column %i allready got",row,column] userInfo:nil]);
    }
}

-(Status *) getStatus{
    Status * st=[[Status alloc] init];
    if([self isDraw]){
        [st setState:Draw];
        return st;
    }else{
        Sings *theWinner=[[Sings alloc] init];
        //RIGHE
        for(int row=0; row<3; row++){
            if(  [[board getSign:row col:0] isItBlank] == NO &&
                 [[board getSign:row col:1] isItBlank] == NO &&
                 [[board getSign:row col:2] isItBlank] == NO &&
                [[board getSign:row col:0] isEqualTo: [board getSign:row col:1]]&&
                [[board getSign:row col:0] isEqualTo: [board getSign:row col:2]]){
                theWinner= [board getSign:row col:0];
                break;
            }
        }
            
        // COLONNE
        for(int col=0; col<3; col++){
            if(  [[board getSign:0 col:col] isItBlank] == NO &&
               [[board getSign:1 col:col] isItBlank] == NO &&
               [[board getSign:2 col:col] isItBlank] == NO &&
               [[board getSign:0 col:col] isEqualTo: [board getSign:1 col:col]]&&
               [[board getSign:0 col:col] isEqualTo: [board getSign:2 col:col]]){
                theWinner= [board getSign:0 col:col];
                break;
            }
        }
        
        //DIAGONALE 
        
        if([[board getSign: 0 col: 0] isItBlank] == NO &&
           [[board getSign: 1 col: 1] isItBlank] == NO &&
           [[board getSign: 2 col: 2] isItBlank] == NO &&
           [[board getSign:0 col:0] isEqualTo:[board getSign:1 col:1]]&&
           [[board getSign:1 col:1] isEqualTo:[board getSign:2 col:2]]){
            theWinner= [board getSign:0 col:0];
        }
        
        if([[board getSign: 0 col: 2] isItBlank] == NO &&
           [[board getSign: 1 col: 1] isItBlank] == NO &&
           [[board getSign: 2 col: 0] isItBlank] == NO &&
           [[board getSign:0 col:2] isEqualTo:[board getSign:1 col:1]]&&
           [[board getSign:1 col:1] isEqualTo:[board getSign:2 col:0]]){
            theWinner= [board getSign:0 col:2];
        }
        
        switch ([theWinner segno]){
            case Cross:{
                [st setState:CrossWin];
            }break;
            case Circle:{
                [st setState:CircleWin];
            }break;
            default:{
                [st setState:GameOpen];
            }
        }
            
        return st;
    }
    return nil;
}

-(IBoard *) clone{
    TwoDArray *boardCopy=[[TwoDArray alloc]init];
    
    for(int row=0; row<3;row++){
        for(int column=0; column<3; column++){
            [boardCopy setIn:row col:column val:[board getSign:row col:column]];
        }
    }
    return [[IBoard alloc] initWithBoard:boardCopy];
}

-(void) checkValue: (int)x versus: (NSString *) v{
    if(!(x <= 3) && !(x >= 0)){
        NSString *error= [NSString stringWithFormat:@"%@ must be between 0-2", v];
        @throw([NSException exceptionWithName:@"InvalidRowParameter" reason:error userInfo:nil]);
    }
}

-(BOOL) isDraw{
    int valNotNull=0;
    for( int row=0; row<=2; row++){
        for(int column=0; column <=1; column++){
            if([[board getSign: row col: column] isItBlank] == NO) {
                valNotNull++;
            }
        }
    }
    return valNotNull==9;
}

@end