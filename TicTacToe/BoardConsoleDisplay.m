//
//  BoardConsoleDisplay.m
//  TicTacToe
//
//  Created by Lion User on 20/09/2012.
//  Copyright (c) 2012 MensAgilis. All rights reserved.
//

#import "BoardConsoleDisplay.h"

@implementation BoardConsoleDisplay

@synthesize board;

-(void) display{
    NSMutableString * buffer= [NSMutableString stringWithFormat:@"\r ==== THIS GAME ==== \r\r"];
    if(board!= nil){
        for(int row=0; row<3; row++){
            for(int column=0; column<3; column++){
                Sings *cell = [board getCell:row in:column];
                    switch ([cell segno]) {
                        case Circle:
                            [buffer appendFormat:@" O "];
                            break;
                        case Cross:
                            [buffer appendFormat:@" X "];
                            break;
                        default:
                            [buffer appendFormat:@"   "];
                            break;
                    }
                if(column <3){
                    [buffer appendFormat:@"|"];
                }
            }
            [buffer appendFormat:@"\r"];
            if(row <3){
                [buffer appendFormat:@"---------------------\r"];
            }
        }
        NSLog(@"%@",buffer);
    }
}

@end
